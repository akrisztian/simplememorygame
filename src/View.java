import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class View extends JFrame {

    private Model model;
    private JPanel jPanel;
    private JLabel label1;
    private JLabel label2;
    private JToggleButton[] buttons;

    public View(String title, Model model) throws HeadlessException {
        super(title);
        this.model = model;


        this.setBounds(0,0,250,250);
        this.setLocationRelativeTo(null);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        buttons = new JToggleButton[9];
        jPanel = new JPanel();

        jPanel.setLayout(new GridLayout(3,3));

        //add buttons to panel
        for(int i = 0; i<9; i++){
            buttons[i] = new JToggleButton(String.valueOf(i+1));

            int finalI1 = i;
            buttons[i].addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if(e.getStateChange() == ItemEvent.SELECTED){
                        model.setSelectedIndexTrue(finalI1);
                        printLabel();
                    } else if(e.getStateChange() == ItemEvent.DESELECTED){
                        model.setSelectedIndexFalse(finalI1);
                        printLabel();
                    }
                }
            });



            jPanel.add(buttons[i]);
        }

        label1 = new JLabel(" ");
        label2 = new JLabel(" ");

        add(label1,BorderLayout.NORTH);
        add(label2,BorderLayout.SOUTH);
        add(jPanel,BorderLayout.CENTER);

        setVisible(true);
    }

    public JToggleButton[] getButtons() {
        return buttons;
    }

    public void setButtons(JToggleButton[] buttons) {
        this.buttons = buttons;
        this.repaint();
    }

    public void setLabel1(String text){
        label1.setText(text);
        this.repaint();
    }

    public void setLabel2(String text){
        label2.setText(text);
        this.repaint();
    }

    public JLabel getLabel1() {
        return label1;
    }

    public JLabel getLabel2() {
        return label2;
    }

    //show game over dialog
    public void showDialog(){
        int result = JOptionPane.showConfirmDialog(null,"Perfect rounds :" + model.getPerfectRounds(),"Game Over",JOptionPane.DEFAULT_OPTION);
        if(result == JOptionPane.DEFAULT_OPTION || result == JOptionPane.OK_OPTION){
            System.exit(0);
        }
    }

    //print selected values
    private void printLabel(){
        Boolean[] selected = model.getIsSelected();
        label2.setText(" ");
        for(int i=0; i<9; i++){
            if(selected[i]){
                label2.setText(label2.getText() + " " + (i+1));
            }
        }
    }
}
