import java.util.Arrays;

public class Model {

    private Boolean[] isSelected;       //selected buttons
    private int perfectRounds;          //number of perfect rounds

    public Model() {
        isSelected = new Boolean[9];
        perfectRounds = 0;

        setSelectedFalse();
    }

    //set all values to false
    public void setSelectedFalse(){
        Arrays.fill(isSelected,false);
    }

    //set specific value to true
    public void setSelectedIndexTrue(int ind){
        isSelected[ind] = true;
    }

    //set specific value to false
    public void setSelectedIndexFalse(int ind){
        isSelected[ind] = false;
    }

    public Boolean[] getIsSelected() {
        return isSelected;
    }

    public void setPerfectRounds(int perfectRounds) {
        this.perfectRounds = perfectRounds;
    }

    public int getPerfectRounds() {
        return perfectRounds;
    }
}
