import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Controller extends Thread {

    private Model model;
    private View view;
    private int roundNo;
    private int perfectRoundNo;
    private ArrayList<Integer> numbers;

    public Controller(Model model, View view, int roundNo) {
        this.model = model;
        this.view = view;
        this.roundNo = roundNo;

        numbers = new ArrayList<>(Arrays.asList(0,1,2,3,4,5,6,7,8));

        perfectRoundNo=0;
    }

    @Override
    public void run() {
        super.run();

        JToggleButton[] buttons = view.getButtons();
        Boolean[] selected = new Boolean[9];
        int correctNo = 0;

        for(int i=0; i< roundNo; i++){

            //disable buttons at round start
            disableButtons();

            //no numbers selected at round start
            model.setSelectedFalse();
            Arrays.fill(selected,false);

            //set 2. label to " "
            view.setLabel2(" ");

            //select 5 random numbers and write to label
            Collections.shuffle(numbers);
            for(int j=0; j<5; j++){
                selected[numbers.get(j)] = true;
                view.setLabel1(view.getLabel1().getText() + " " + (numbers.get(j)+1));
            }


            //sleep 2 seconds
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            view.setLabel1(" ");
            enableButtons();

            //sleep 4 seconds
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }

            //correct numbers 0
            correctNo = 0;

            //get selected buttons from model
            Boolean[] modelSelected = model.getIsSelected();

            //count correct numbers
            for(int j=0; j<9; j++){
                if(modelSelected[j] && selected[j]){
                    correctNo++;
                }
            }

            if(correctNo == 5){
                perfectRoundNo++;
            }

        }

        model.setPerfectRounds(perfectRoundNo);
        view.showDialog();

    }

    private void disableButtons(){
        JToggleButton[] buttons = view.getButtons();
        for(int i = 0; i<9; i++){
            buttons[i].setEnabled(false);
            buttons[i].setSelected(false);
        }
        view.setButtons(buttons);
    }

    private void enableButtons(){
        JToggleButton[] buttons = view.getButtons();
        for(int i = 0; i<9; i++){
            buttons[i].setEnabled(true);
        }
        view.setButtons(buttons);
    }
}
