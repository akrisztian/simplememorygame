
public class Main {

    public static void main(String[] args) {
        int roundNo=3;

        //check parameter
        try {
            roundNo = Integer.parseInt(args[0]);

            if(roundNo <= 0){
                System.err.println("roundNo <= 0; using default value(3)");
                roundNo = 3;
            }
        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
            System.err.println("no parameter found; using default value(3)");
            roundNo = 3;
        } catch (NumberFormatException e){
            e.printStackTrace();
            System.err.println("NumberFormatException while parsing roundNo; using default value(3)");
        }


        //create game
        Model m = new Model();
        View v = new View("SimpleMemoryGame",m);
        new Controller(m,v,roundNo).start();
    }
}
