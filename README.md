# Simple Memory Game
A simple memory game written in Java, made to exercise MVC pattern

## General Info

The game displays a set of numbers at the top of the screen, after which the numbers disappear, and the player has a set amount of time to push all the correct buttons. After all the rounds are done, the game shows the amount of perfect rounds the player got (rounds in which the player got all numbers right).
  
The player can push only as many buttons as the game displayed, so that the player can not push all 9 buttons and win every turn.

## Screenshots

![Game Screen](./images/game.png)

## Usage

The number of rounds should be given as a command line argument. The default value is 3.